// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <windows.h>

#include "body.h"

#include <debugger/debug.h>

extern "C" void __stdcall GetLibraryBody(int *d,int *dd);

void CBody::GetBody(const char* dllname)
{
  DBGTRACE("before make body\n");
  int start1,end1;
  GetLibraryBody(&start1,&end1);
  DBGTRACE("maked code %d\n",end1-start1);

  const unsigned char* start;
  int size;
  GetLoadLibraryA(&start,&size);

  m_body.clear();
  m_body.insert(m_body.end(),start,start+size);
  m_body.insert(m_body.end(),reinterpret_cast<const unsigned char*>(dllname),reinterpret_cast<const unsigned char*>(strchr(dllname,0)+1));

  DBGTRACE("maked total %d\n",m_body.size());
}

void CBody::Write(HANDLE hProcess,void* entrypiont,const char* dllname)
{
  m_process=hProcess;
  m_entrypoint=(DWORD)entrypiont;

  GetBody(dllname);

  DBGTRACE("infect %d %08x,'%s' witch %d\n",hProcess,entrypiont,dllname,m_body.size());

  DWORD old;
  VirtualProtectEx(hProcess,(LPVOID)entrypiont,m_body.size(),PAGE_EXECUTE_READWRITE,&old);
  DWORD read,write;
  m_bodybak.resize(m_body.size());
  ReadProcessMemory(hProcess,(LPVOID)entrypiont,&m_bodybak.front(),m_bodybak.size(),&read);
  WriteProcessMemory(hProcess,(LPVOID)entrypiont,&m_body.front(),m_body.size(),&write);
  VirtualProtectEx(hProcess,(LPVOID)entrypiont,m_body.size(),old,&old);
}

void CBody::Restore()
{
  DWORD write;
  WriteProcessMemory(m_process,(LPVOID)m_entrypoint,&m_bodybak.front(),m_bodybak.size(),&write);
}

#pragma optimize("",off)
void CBody::GetLoadLibraryA(const unsigned char** lpStart,int* uiSize)
{
  __asm
  {
    mov eax,lpStart;
    mov dword ptr [eax],offset _start;
    mov ebx,offset _end;
    sub ebx,offset _start;
    mov eax,uiSize;
    mov dword ptr [eax],ebx
  }
  return;

_start:
  typedef struct seh_tag
  {     
    seh_tag  *seh;
    DWORD  pExcFunction;
  } seh_t;

  seh_t *seh;
  DWORD lpDosHeader;
  PIMAGE_DOS_HEADER pidh;
  PIMAGE_NT_HEADERS pinh;
  PIMAGE_EXPORT_DIRECTORY lpExport;
  PIMAGE_THUNK_DATA pitd;
  DWORD lpFuncStart;
  LPCSTR lpName;
  LPWORD lpOrdinal;
  DWORD lpLoadLibrary;

  // чтобы переменные указывали на реальный адрес
  _asm
  {
    // push ebp;
    mov ebp,esp;

    /*
    push eax;
    push ebx;
    push ecx;
    push edx;
    push esi;
    push edi;
    pushf;
    */

    sub esp,40
  }

  __asm
  {
    mov eax,fs:[0];
    mov [seh],eax;
  }
  while((DWORD)seh->seh != 0xffffffff)
     seh= seh->seh;   
  lpDosHeader=(seh->pExcFunction)&0xffff0000;
  while(*(WORD*)lpDosHeader!='ZM')
    lpDosHeader-=0x10000;
  pidh=(PIMAGE_DOS_HEADER)lpDosHeader;
  pinh=(PIMAGE_NT_HEADERS)(
    (DWORD)pidh+
    (DWORD)pidh->e_lfanew);
  lpExport=(PIMAGE_EXPORT_DIRECTORY)(
    (DWORD)pinh->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress+
    pinh->OptionalHeader.ImageBase);
  pitd=(PIMAGE_THUNK_DATA)(lpExport->AddressOfNames+
    pinh->OptionalHeader.ImageBase);
  lpFuncStart=lpExport->AddressOfFunctions+
    pinh->OptionalHeader.ImageBase;
  lpName=(LPCSTR)(pitd->u1.ForwarderString+
    pinh->OptionalHeader.ImageBase);
  lpOrdinal=(LPWORD)((*lpExport).AddressOfNameOrdinals+
    pinh->OptionalHeader.ImageBase);
  while(*(LPDWORD)lpOrdinal!=0)
  {
    //LoadLibraryA
    //Ayra rbiL daoL
    if(*(DWORD*)lpName=='daoL'&&*(DWORD*)(lpName+4)=='rbiL'&&*(DWORD*)(lpName+8)=='Ayra')
    //MessageBoxA
    //Axo Bega sseM
    //if(*(DWORD*)lpName=='sseM'&&*(DWORD*)(lpName+4)=='Bega'&&*(DWORD*)(lpName+8)=='Axo')
    {
      lpLoadLibrary=*(DWORD*)(lpFuncStart+(*lpOrdinal)*4)+pinh->OptionalHeader.ImageBase;
      __asm
      {
        call $+5;
    _geteip:
        pop eax;
        add eax,offset _end;
        sub eax,offset _geteip;
        push eax;
        sub eax,offset _end;
        add eax,offset _start;
        push eax; // адрес возврата, указывает на начало кода _start
        jmp [lpLoadLibrary];
      }
    }

    lpOrdinal++;
    while(*lpName!=0)
    {
      lpName++;
    };
    lpName++;
  }

  // этот код не должен выполняться, выполнение сюда должно попасть
  // по переходу на _run;

  /*

  // восстанавливую стек
  __asm
  {
    add esp,40; // push 0 * 10
  }

  __asm
  {
    popf;
    pop edi;
    pop esi;
    pop edx;
    pop ecx;
    pop ebx;
    pop eax;

    pop ebp;
  }

  */

_end: ;
}
