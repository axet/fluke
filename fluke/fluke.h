// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/// @file
/// @author Alexey Kuznetsov (axet@nm.ru)

/// модуль встраивание во внешнее НЕ загружонное приложение.

/// Спомощью этого модуля можно легко подключиться
/// к любому процессу приложения, перехватывать обращения к люым библиотечным фукнциям, контролировать передачупараметров,
/// заменять их, отписывать события на сторону контролирующей стороны. Так же имеються фукнции для перехвата
/// внутренних фукнций приложения и управления ими , тех фукнций которые не являються 
/// билиотекчными, не экпортируемых, предварительно найденых спомощью дизассемблеров или отладчика.
/// метод инфицирования основан на подгрузке бибилиотеки, собранной на высокоуровневом языке, таком как с++, VB, java.
/// сам процесс инфцицированя реализован на подставлении тела вируса написанного на асемблере, в код инфицируемого
/// процесса.
namespace Fluke
{
  class FlukeException:public std::exception
  {
    std::string str;
  public:
    FlukeException(const char*p)
    {
      str=p;
    }
    const char* what() const throw()
    {
      return str.c_str();
    }
  };

  extern const char g_flukeslaveready[];
};

#include "flukemaster.h"
#include "flukeslave.h"

#ifdef _DEBUG
#pragma comment(lib,"fluke.lib")
#else
#pragma comment(lib,"fluke.lib")
#endif
