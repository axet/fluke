// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Fluke.cpp : Defines the entry point for the DLL application.
//

#include <windows.h>

#include <string>
#include <sstream>

#include "sync.h"
#include "fluke.h"

#include <debugger/debug.h>

CFlukeSlave::CFlukeSlave():m_globalpass(0),m_library(0)
{
}

HMODULE CFlukeSlave::GetCurrentModuleHandle()
{
  DWORD thismod;
  __asm
  {
    call $+5;
    pop eax;
    and eax,0FFFFF000h;
    mov thismod,eax;
  }
  while(*(WORD*)thismod!='ZM')
    thismod-=0x1000;

  return (HMODULE)thismod;
}

void CFlukeSlave::flukeslave(HMODULE hmod)
{
  DBGTRACE("CFlukeSlave::flukeslave\n");
  CSyncClient sync1("IOFlukeApp",GetCurrentThreadId());
  DBGTRACE("sync1.IsExist()==%s\n",sync1.IsExist()?"true":"false");
  if(!sync1.IsExist())
    return;

  char name[MAX_PATH];
  DWORD sname=GetModuleFileName(hmod,name,sizeof(name));
  if(sname==0||(sname==sizeof(name)))
    throw FlukeException("Path Test");
  DBGTRACE("LoadLibrary(%s)\n",name);
  m_library=LoadLibrary(name);

  CSyncServer sync2("IOFlukeDll",GetCurrentThreadId());

  DBGTRACE("sync1.RunClient()\n",hmod);
  sync1.RunClient();

  DBGTRACE("sync2.WaitClient()\n",hmod);
  sync2.WaitClient();

  std::ostringstream str;
  str<<g_flukeslaveready<<GetCurrentThreadId();
  DBGTRACE("m_globalpass: %s\n",str.str().c_str());
  m_globalpass=CreateMutex(0,TRUE,str.str().c_str());

  DBGTRACE("InitInstance(%08x)\n",hmod);
  FlukeInit(hmod);
}

CFlukeSlave::~CFlukeSlave()
{
  Close();
}

void CFlukeSlave::Create(HMODULE h)
{
  DBGTRACE("FlukeInit(%08x)\n",h);
  if(h==0)
    h=GetCurrentModuleHandle();
  flukeslave(h);
}

void CFlukeSlave::Close()
{
  if(m_globalpass!=0)
  {
    CloseHandle(m_globalpass);
    m_globalpass=0;
  }
  if(m_library!=0)
  {
    FreeLibrary(m_library);
  }
}
  
FLUKE_EXPORT(LRESULT) FlukeSlaveHook(int code,WPARAM wParam,LPARAM lParam)
{
  DBGTRACE("FlukeSlaveHook\n");
  return 0;//CallNextHookEx(0,code,wParam,lParam);
}
