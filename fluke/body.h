// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <string>
#include <vector>

namespace Fluke{

  /// Тело експлоита которое передаеться в процесс
  class CBody
  {
    /// бинарный массив
    typedef std::vector<unsigned char> array;

    /// хендл процесса подвергающегося инфицированию
    HANDLE m_process;
    /// точка входа в мейн процесса.
    DWORD m_entrypoint;
    /// тело вируса и код который необходимо восстановить
    std::vector<unsigned char> m_body,m_bodybak;

    /// функция возвращает тело, в нем есть код для поиска loadlibrary
    static void GetLoadLibraryA(const unsigned char** lpStart,int* uiSize);
    /// инфицировать исполняемый файл, передав ему имя библиотеки, которая будет присоеденена к процессу
    void GetBody(const char* dllname);
  public:
    /// инфицирование указанного процесса и загрузка указанной билиотеки в процесс.
    /// @param hProcess хендел процесса для инфицирования
    /// @param entrypoint точка входа в процесс, куда необходимо класть тело вируса.
    /// @param dllname бибилиотека которую нужно подгрузить

    /// точка входа не определяеться динамически для того чтобы можно было инфицировать любой, даже
    /// запущенный процесс, это возможно сделлать несколькими путями: первый - это подстановка в точку входа
    /// исполняемого файла или библиотеки, но делать это нужно до запуска приложения, к примеру если мы его
    /// создаем через CreateProcesss. второй путь - это остановка процесса через ThreadSuspend() и подстановка
    /// entrypoint как точку обработки сообщений DefWindowProc процесса, третий - через установку любой поплулярной
    /// фукнции WinAPI, четвертый - через определения текущей позиции eip процесса и вставление кода туда... и 
    /// это далеко не последний способ :)
    void Write(HANDLE hProcess,void* entrypiont,const char* dllname);

    /// востановить инфицированный процесс, и вернуть ему прежнюю функциональность.
    void Restore();
  };
}

using namespace Fluke;
