; GZProject - library, Ultima Online utils.
; Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

; This program is free software; you can redistribute it and/or
; modify it under the terms of the GNU General Public License
; as published by the Free Software Foundation; either version 2
; of the License, or (at your option) any later version.

; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with this program; if not, write to the Free Software
; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

option casemap:none
.386
.model flat,stdcall
include c:/masm32/include/windows.inc

.code

LoadLibraryBody proto

GetLibraryBody proc public uses eax ebx,
  procstart:DWORD,
  procend:DWORD

  mov eax,procstart
  mov [eax],offset LoadLibraryBodyStart
  mov eax,procend
  mov ebx,offset LoadLibraryBodyEnd
  mov [eax],ebx
  ;invoke LoadLibraryBody
  ret
GetLibraryBody endp

seh_t STRUCT
  seh DWORD ? ; next seh_t
  pExcFunction DWORD ?
seh_t ENDS

; ��������� �� ������ ���������� ���������� ���������� ����.
; �� ������ �� ������ ������� ������� ���������

LoadLibraryBodyStart:
LoadLibraryBody proc USES ebx esi edi
  LOCAL run_namelibrary:DWORD
  LOCAL run_returnaddress:DWORD
  LOCAL seh:DWORD
  LOCAL imagebase:DWORD
  LOCAL lpFuncStart:DWORD
  LOCAL lpOrdinal:DWORD
  LOCAL lpName:LPCSTR
  LOCAL lpLoadLibrary:DWORD

@@start:

  assume fs: nothing
  mov eax,fs:[0]
  mov seh,eax

  mov esi,seh
  @@:  
  cmp esi,-1
  jz @F
  mov seh,esi
  mov esi,(seh_t ptr[esi]).seh
  jmp @B
  @@:

  ; ��� ������ ������
  mov esi,seh
  mov eax,(seh_t ptr[esi]).pExcFunction
  and eax,0ffff0000h
  .while (WORD ptr [eax]) != 05a4dh ; MZ 4d 5a
    sub eax,10000h
  .endw

  mov esi,eax
  ;esi = IMAGE_DOS_HEADER
  mov edi,esi
  add edi,(IMAGE_DOS_HEADER ptr[esi]).e_lfanew;
  ; edi = IMAGE_NT_HEADERS
  mov eax,(IMAGE_NT_HEADERS ptr[edi]).OptionalHeader.ImageBase
  mov imagebase,eax
  mov esi,(IMAGE_NT_HEADERS ptr[edi]).OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress
  add esi,imagebase
  ; esi = IMAGE_EXPORT_DIRECTORY
  mov eax,(IMAGE_EXPORT_DIRECTORY ptr[esi]).AddressOfFunctions
  add eax,imagebase
  mov lpFuncStart,eax
  mov edi,(IMAGE_EXPORT_DIRECTORY ptr[esi]).AddressOfNames
  add edi,imagebase
  ; esi = IMAGE_THUNK_DATA
  mov eax,(IMAGE_THUNK_DATA ptr[edi]).u1.ForwarderString
  add eax,imagebase
  mov lpName,eax

  mov eax,(IMAGE_EXPORT_DIRECTORY ptr[esi]).AddressOfNameOrdinals
  add eax,imagebase
  mov lpOrdinal,eax
  
  ; esi = ORDINAL
  .while 1
    jmp @F
    textLoadLibraryA db "LoadLibraryA",0
    @@:
    mov esi,lpName
    mov edi,offset textLoadLibraryA
    @@:
	cmp byte ptr[esi],0
  	.if ZERO?
  	  cmpsb
    .elseif
      cmpsb
      jz @B
    .endif

    @@:
    .if ZERO?
      mov eax,lpOrdinal
	  mov ax,word ptr [eax]
	  and eax,00000ffffh
	  add eax,eax
	  add eax,eax
	  add eax,eax
      add eax,lpFuncStart
	  mov eax,[eax]
      add eax,imagebase
      mov lpLoadLibrary,eax
      call @@geteip
      @@geteip:
      pop ebx;
      add ebx,offset @@end;
      sub ebx,offset @@geteip;
      mov run_namelibrary,ebx;
      sub ebx,offset @@end;
      add ebx,offset @@start;
      mov run_returnaddress,ebx;  ����� ��������, ��������� �� ������ ���� _start
      ; ebx esi edi
      pop edi
      pop esi
      pop ebx
      leave
      jmp eax
    .endif

    ; ���� ��������� ���
    mov edi, lpName
    mov al,0
    repnz scasb
    mov lpName,edi
    ; ���� �������� �������
    inc lpOrdinal
  .endw
  ret
@@end:
LoadLibraryBody endp
LoadLibraryBodyEnd:

end
