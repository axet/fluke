// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "windows.h"

#include "sync.h"
#include "interprocexception.h"

#include <sstream>

#include <ErrorReport/ErrorReport.h>

/////////////////////////////// 

CSyncServer::CSyncServer(const char* name,DWORD ClientPID):m_server(0)
{
  char buf[64];
  _itoa(ClientPID,buf,10);
  m_eventname=name;
  m_eventname+=buf;
  CreateServer();
}

CSyncServer::~CSyncServer()
{
  if(m_server!=0)
    CloseHandle(m_server);
}

void CSyncServer::CreateServer()
{
  SECURITY_ATTRIBUTES sa={sizeof(sa)};
  sa.bInheritHandle=TRUE;
  sa.lpSecurityDescriptor=0;
  m_server=CreateEvent(&sa,TRUE,FALSE,m_eventname.c_str());
  int err=GetLastError();
  if(err==ERROR_ALREADY_EXISTS)
    ResetEvent(m_server);

  if(m_server==0)
    throw interprocexception(ErrorReport("can't create event",m_eventname.c_str())+
    ErrorReport(GetLastErrorString(err).c_str()));
}

void CSyncServer::WaitClient()
{
  if(WaitForSingleObject(m_server,10000)==WAIT_TIMEOUT)
    throw interprocexception("CSyncServer::WaitClient - Time Out");
  CloseHandle(m_server);
  m_server=0;
}

///////////////

CSyncClient::CSyncClient(const char* name,DWORD ClientPID)
{
  char buf[64];
  _itoa(ClientPID,buf,10);
  m_eventname=name;
  m_eventname+=buf;
}

void CSyncClient::RunClient()
{
  HANDLE iofluke=OpenEvent(EVENT_MODIFY_STATE,FALSE,m_eventname.c_str());
  if(iofluke==0)
    throw interprocexception(ErrorReport("can't open event for release",m_eventname.c_str())+
    ErrorReport(GetLastErrorString().c_str()));
  SetEvent(iofluke);
  CloseHandle(iofluke);
}

bool CSyncClient::IsExist()
{
  HANDLE iofluke=OpenEvent(EVENT_MODIFY_STATE,FALSE,m_eventname.c_str());
  if(iofluke==0)
    return false;
  CloseHandle(iofluke);
  return true;
}
