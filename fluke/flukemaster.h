// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifdef _DEBUG
#pragma comment(lib,"fluke.lib")
#else
#pragma comment(lib,"fluke.lib")
#endif

namespace Fluke
{
  /// класс контролер инфицирого приложения
  class CFlukeMaster
  {
    /// определяет точку входа в ЕХЕ по указанному пути
    static DWORD GetEntryPoint(const char* apppath);
  public:
    /// работает с приложением запускаяась после отработки приложения
    /// возвращают ProcessId
    static DWORD flukemaster(const char* app,const char* cmd,const char *dllname);

    /// работает с приложением запускаяась после отработки приложения
    static bool flukemaster(unsigned threadid,const char *dllname);

    /// работает для приложения, загружаясь до его запуска
    /// возвращают ProcessId
    static DWORD flukemaster_instant(const char* app,const char* cmd,const char *dllname);

    /// вспомогательная фукнция для инфицирования приложения которое мы сами запускаем.

    /// запускаем мы его через CreateProcess(CREATE_SUSPENDED). и прописываемся в exe_entry_point
    static DWORD CFlukeMaster::flukemaster_instant_infect(const char* app,const char* cmd,const char *dllname);
    /// Запускаем вирус через подмену импортируемой длл.
    
    /// данный способ нужен если приложение защищено от
    /// других способов инфицирования.
    /// <PRE>
    /// 1. заменить таблицу импорта ссылкой на свою бибилотеку и левую функцию
    /// 2. запустить код
    /// 3. на удаленной стороне восстановить таблицу импорта из файла и
    ///    разпарсить ее
    /// 4. передать управление точки входа екзешника.
    /// всю работу нужно делать в родном потоке, а контроль осуществлять в
    /// новом.
    /// </PRE>
    static DWORD CFlukeMaster::flukemaster_instant_import(const char* app,const char* cmd,const char *dllname);
  };
}
using namespace Fluke;
