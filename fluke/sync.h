// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef __SYNC_H__
#define __SYNC_H__

#include <string>
#include <exception>

namespace Fluke{
  
  /// класс для сонхронизации мастера со слейвом
  class CSyncServer
  {
    std::string m_eventname;
    HANDLE m_server;

    ///создает приложение которое ждет запуска клиента
    void CreateServer();
  public:
    CSyncServer(const char* name,DWORD ClientPID);
    ~CSyncServer();

    /// начать ожидания клиента
    void WaitClient();
  };

  /// класс для синзронизации слейва с мастером.
  class CSyncClient
  {
    std::string m_eventname;
  public:
    CSyncClient(const char* name,DWORD ClientPID);
    /// проверяет ждет ли меня сервер?
    bool IsExist();
    ///создает приложение которое ставит галку что клиент котов, сервер
    ///продолжает выполнение
    void RunClient();
  };
}
using namespace Fluke;

#endif
