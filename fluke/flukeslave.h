// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef __FLUKESLAVE_H__
#define __FLUKESLAVE_H__

#include <windows.h>

#ifdef _DEBUG
#pragma comment(lib,"fluke.lib")
#else
#pragma comment(lib,"fluke.lib")
#endif

namespace Fluke{
  /// должен жить пока загружена Длл (глобальный класс).

  /// в нем есть мутекс котороый говорит что этот процесс успешно подсоеденен. объект создаеться когда 
  /// инфицированная длл уже загружна, она должна создать один экземпляр CFlukeSlave и он информирует
  /// основной процесс (масера) о том что длл подгружена и можно вернуть основному процессу нормльную
  /// фукнциональност, те можно убить вирус из кода процесса и продолжить испольнения с той точки где
  /// он был прерван.
  class __declspec(novtable) CFlukeSlave
  {
    /// mutex который говорит что длл успешно загружена
    /// FLukeMaster сначало проверит этот фалг а только оптом
    /// будет пробовать ожидать клиента
    HANDLE m_globalpass;
    /// Хендл на бибилотеку которую мы держим (защищаем сами себя от выгрузки)
    HMODULE m_library;

    void flukeslave(HMODULE);
    /// возврващет текущий хендл, чтобы можно было найдти имя длл которая создала экземлпяр 
    /// этого классса
    static HMODULE GetCurrentModuleHandle();

    /// если инициализация флукеслейва проходит успешно данный метод будет вызнан
    /// что означает - главному приложению можно инициализироваться
    virtual void FlukeInit(HMODULE) {};
  public:
    CFlukeSlave();
    ~CFlukeSlave();

    /// лучше конечно делать конструктор, это нагладнее, но в этом случае
    /// не будет работать виртульный вызов FlukeInit для приложения
    void Create(HMODULE h = 0);
    /// когда длл выгнужаеться нужно закрыть хендлы.
    void Close();
  };
}

#ifdef _FLUKE_EXPORT
#define FLUKE_EXPORT(x) extern "C" __declspec(dllexport) x
#else
#define FLUKE_EXPORT(x) extern "C" __declspec(dllimport) x
#endif

FLUKE_EXPORT(LRESULT) FlukeSlaveHook(int code,WPARAM wParam,LPARAM lParam);

using namespace Fluke;

#endif
