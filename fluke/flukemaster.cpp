// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <windows.h>

#include <iostream>
#include <fstream>
#include <sstream>

#include "sync.h"
#include "body.h"
#include "fluke.h"

#include <debugger/debug.h>
#include <ErrorReport/ErrorReport.h>

DWORD CFlukeMaster::GetEntryPoint(const char* apppath)
{
  std::ifstream file(apppath,std::ios::binary|std::ios::in);
  if(!file.is_open())
    throw FlukeException(ErrorReport("no entry point process",apppath));
  IMAGE_DOS_HEADER idh;
  if(file.read((char*)&idh,sizeof(idh)).gcount()!=sizeof(idh))
    throw FlukeException("bad read on entry point");
  file.seekg(idh.e_lfanew,std::ios::beg);
  IMAGE_NT_HEADERS inh;
  file.read((char*)&inh,sizeof(inh));
  DBGTRACE("Entry point is: %08x\n",inh.OptionalHeader.AddressOfEntryPoint+inh.OptionalHeader.ImageBase);
  return inh.OptionalHeader.AddressOfEntryPoint+inh.OptionalHeader.ImageBase;
}

DWORD CFlukeMaster::flukemaster_instant(const char* app,const char* cmd,const char *dllname)
{
  return flukemaster_instant_infect(app,cmd,dllname);
}

DWORD CFlukeMaster::flukemaster_instant_infect(const char* app,const char* cmd,const char *dllname)
{
  DBGTRACE("flukemaster_instant\n");
  STARTUPINFO si={sizeof si,0};
  PROCESS_INFORMATION pi={0};
  if(!CreateProcess(app, const_cast<char*>(cmd),0,0,0,CREATE_SUSPENDED,0,0,&si,&pi))
  {
    throw FlukeException(ErrorReport("can't create process",app)+
      ErrorReport(GetLastErrorString().c_str()));
  }

  void* entrypoint=(void*)GetEntryPoint(app);

  CBody body;
  HANDLE process=OpenProcess(PROCESS_ALL_ACCESS,FALSE,pi.dwProcessId);
  if(process==0)
    throw FlukeException(GetLastErrorString().c_str());
  body.Write(process,entrypoint,dllname);

  CSyncServer sync1("IOFlukeApp",pi.dwThreadId);
  ResumeThread(pi.hThread);
  sync1.WaitClient();
  DBGTRACE("sync1.WaitClient()\n");
  body.Restore();
  CSyncClient sync2("IOFlukeDll",pi.dwThreadId);
  sync2.RunClient();
  DBGTRACE("sync2.RunClient()\n");

  CloseHandle(process);
  CloseHandle(pi.hProcess);
  CloseHandle(pi.hThread);

  return pi.dwProcessId;
}

/*
DWORD CFlukeMaster::flukemaster_instant_import(const char* app,const char* cmd,const char *dllname)
{
  DBGTRACE("flukemaster_instant_import\n");
  STARTUPINFO si={sizeof si,0};
  PROCESS_INFORMATION pi={0};
  if(!CreateProcess(app, const_cast<char*>(cmd),0,0,0,CREATE_SUSPENDED,0,0,&si,&pi))
  {
    throw exception((std::string("can't create process: ")+app).c_str());
  }

  // 1. заменить таблицу импорта ссылкой на свою бибилотеку и левую функцию
  // 2. запустить код
  
  // 3. на удаленной стороне восстановить таблицу импорта из файла и
  //    разпарсить ее
  // 4. передать управление точки входа екзешника.
  // всю работу нужно делать в родном потоке, а контроль осуществлять в
  // новом.

  HANDLE process=OpenProcess(PROCESS_ALL_ACCESS,FALSE,pi.dwProcessId);

  CSyncServer sync1("IOFlukeApp",pi.dwThreadId);
  ResumeThread(pi.hThread);
  sync1.WaitClient();
  DBGTRACE("sync1.WaitClient()\n");
  body.Restore();
  CSyncClient sync2("IOFlukeDll",pi.dwThreadId);
  sync2.RunClient();
  DBGTRACE("sync2.RunClient()\n");

  CloseHandle(process);
  CloseHandle(pi.hProcess);
  CloseHandle(pi.hThread);

	return pi.dwProcessId;
}
*/

DWORD CFlukeMaster::flukemaster(const char* app,const char* cmd,const char *dllname)
{
  STARTUPINFO si={sizeof si,0};
  PROCESS_INFORMATION pi={0};
  if(!CreateProcess(app, const_cast<char*>(cmd),0,0,0,0,0,0,&si,&pi))
    throw FlukeException(ErrorReport("can't create process",app)+ErrorReport(GetLastErrorString().c_str()));
  WaitForInputIdle(pi.hProcess,-1);
  flukemaster(pi.dwThreadId,dllname);
  return pi.dwProcessId;
}

class LoadLib
{
public:
  HMODULE hookdll;

  LoadLib(const char* p)
  {
    hookdll=LoadLibrary(p);
    if(hookdll==0)
      throw FlukeException(ErrorReport("no dll found",p));
  }
  ~LoadLib()
  {
    FreeLibrary(hookdll);
  }
};

class HookEx
{
public:
  HHOOK hhk;

  HookEx(HMODULE hookdll,DWORD threadid)
  {
    hhk=SetWindowsHookEx(WH_GETMESSAGE,(HOOKPROC)GetProcAddress(hookdll,"FlukeSlaveHook"),hookdll,threadid);
    DBGTRACE("%08x SetWindowsHookEx()\n",hhk);
    if(hhk==0)
      throw FlukeException(ErrorReport("can't set hook procedure",GetLastErrorString().c_str()));
  }
  ~HookEx()
  {
    // unload
    if(!UnhookWindowsHookEx(hhk))
      throw FlukeException(ErrorReport("can't unhook procedure",GetLastErrorString().c_str()));
    DBGTRACE("UnhookWindowsHookEx()\n");
  }
};

bool CFlukeMaster::flukemaster(unsigned threadid,const char *dllname)
{
  // load
  {
    std::ostringstream str;
    str<<g_flukeslaveready<<threadid;
    DBGTRACE("flukemaster load %s\n",str.str().c_str());
    HANDLE h;
    if((h=OpenMutex(MUTEX_ALL_ACCESS,FALSE,str.str().c_str()))!=0)
    {
      DBGTRACE("flukemaster already - quit\n");
      CloseHandle(h);
      // уже подключен к данноме процессу
      return false;
    }
  }

  LoadLib loadlib(dllname);
  CSyncServer sync1("IOFlukeApp",threadid);
  HookEx hookex(loadlib.hookdll,threadid);
  PostThreadMessage(threadid,0,0,0);

  sync1.WaitClient();
  DBGTRACE("sync1.WaitClient()\n");

  CSyncClient sync2("IOFlukeDll",threadid);
  sync2.RunClient();
  DBGTRACE("sync2.RunClient()\n");
  return true;
}
