// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef __INTERPROCEXCEPTION_H__
#define __INTERPROCEXCEPTION_H__

#include <exception>
#include <stdio.h>

namespace Fluke{
  
  /// исклчюение вызваемое воермя связи двух процессов, если проихсодят ошибки в момент
  /// передачи данных.
  class interprocexception:public std::exception
  {
    std::string str;
  public:
    interprocexception(const char*p)
    {
      str=p;
    }
    const char* what()const throw()
    {
      return str.c_str();
    }
  };
}

using namespace Fluke;

#endif