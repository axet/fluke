// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef MEMNGR_H
#define MEMNGR_H

#include <windows.h>
#include <atlbase.h>

#include <map>
#include <vector>
#include <set>

#ifdef _DEBUG
#pragma comment(lib,"iofluke.lib")
#else
#pragma comment(lib,"iofluke.lib")
#endif

namespace IOFluke
{
  class CEmulator;
  class HookList;

  /// аредс в исполняемом коде
  /// тот на что указывает EIP
  typedef unsigned int CodeAddress;

  /// набор функций для работы с памятью
  class CMemMngr
  {
    CEmulator &m_emulator;
    HookList &m_hl;
    typedef std::vector<HMODULE> DllList;
    DllList m_dlllist;
    CComAutoCriticalSection m_cs;

  public:
    CMemMngr();
    virtual ~CMemMngr();

  protected:
    typedef void (*CatchProcEnter)(CMemMngr*,va_list );
    typedef void (*CatchProcLeave)(CMemMngr*,int *retval);
    typedef void (*StubLeave)();
    typedef void (*Proc)();
    /// ловит функцию которая заканичвается ОБЯЗАТЕЛЬНЫМ ret
    /// я могу прочитать параметры такой функции и изменить
    /// возвращемы параметры. скажем
    /// <PRE>
    /// push ebp
    /// mov eax,ecx
    /// ret
    /// </PRE>
    /// перехватиться когда попадет в push ebp и когда будет
    /// находитсья на ret
    void CatchRetFunction(CodeAddress oldaddr,CatchProcEnter funcEnter,CatchProcLeave funcLeave = 0);
    /// ловит функцию которую импортируют
    /// те я могу знать с какими параметрами вызывают ShowWindow
    /// и могу менять результат возрвта
    void CatchImportFunction(const char* funcname,const char* dllname,CatchProcEnter funcEnter,CatchProcLeave funcLeave = 0);
    /// изменяет адрес импортируемой процедуры
    /// удобна тем что работает через dynamic_cast
    void RedirectImportFunction(const char* funcname,const char* dllname,CatchProcEnter funcEnter,CatchProcLeave funcLeave,Proc stubLeave);
    void PatchProgram(unsigned long addr,const void* buf,int size);

  private:

  #pragma pack(push,1)

    struct Jmp;
    struct EmulatorBegin;

    /// эта структура записывается в тело старой продецуры
    struct Jmp
    {
      unsigned char push_offset;
      EmulatorBegin* e;
      unsigned char ret;
      Jmp(EmulatorBegin* _e = 0):push_offset(0x68),ret(0xc3),e(_e) {}
    };

    /// функция емулятор входа
    ///   сюда попадаю когда кто-то делает вызов перехватываемой функции
    ///   в стеке лежат все параметры и адрес возврата
    /// после выполнения емулятора переписывую адрес возврата, чтобы перехватить
    ///   результат выполнения, и устанавливает его на EmulatorEnd
    struct EmulatorBegin
    {
      const unsigned char push_dword_jmp_t; 
      /// адрес старой процедуры, которую перехватывали
      CodeAddress retaddr; 
      unsigned char push_dword_client; 
      /// указатель на класс который создает перехватываемый емултор
      CMemMngr* client;
      const unsigned char push_virtual_ret; 
      const unsigned int virtual_ret; 
      const unsigned char push_catch; 
      /// процедура которая отвечает за перехват (она вызовет funcEnter())
      Proc CallCatch; 
      const unsigned char ret; 

      /// данные которые были записаны в начале старой процедуры
      Jmp backdata;
      /// перехват входа (какая процедура вызовиться при входе в перехватываемую функцию
      CatchProcEnter funcEnter;
      /// перехват выхода (какая процедура вызовиться при выходе из перехватываемой функции
      CatchProcLeave funcLeave;

      EmulatorBegin():push_dword_client(0x68),push_catch(0x68),
        push_dword_jmp_t(0x68),ret(0xc3),virtual_ret(0),push_virtual_ret(0xe8) {}
      ~EmulatorBegin()
      {
        *(Jmp*)retaddr=backdata;
      }
    };

    /// на эту структуру ссылаеться адрес взоврата
    struct EmulatorEnd
    {
      const unsigned char push_dword_jmp_t; 
      /// адрес возврата, туда должна была попасть старая функция по своему ret
      CodeAddress retaddr;
      const unsigned char push_dword_client;
      /// this
      CMemMngr* client;
      const unsigned char push_virtual_ret;
      const unsigned int virtual_ret;
      const unsigned char push_catch;
      /// функция перехватат Она вызывает funcLeave
      Proc CallCatch; 
      const unsigned char ret; 

      /// адрес старой процедуры, чтобы можно было вызвать EnableEmulator
      CodeAddress catch_addr; 
      /// адрес функции которя хочет слушать
      CatchProcLeave funcLeave;
      /// адрес в стеке где лежит указатель на этот емулятор
      /// (или в нормальном случае указатель на ардес где записан адрес
      /// процедуры которая должена выоплнитья когда перехватываемая
      /// фукнция звершит работу)
      /// мне необходима возможность отменить действие этого эмулятора
      /// если эмулятор не активен  stacklink == 0
      CodeAddress* stacklink;

      EmulatorEnd():push_dword_client(0x68),push_catch(0x68),
        push_dword_jmp_t(0x68),ret(0xc3),virtual_ret(0),push_virtual_ret(0xe8),
        stacklink(0) {}
      ~EmulatorEnd()
      {
        if(stacklink!=0)
        {
          *stacklink=retaddr;
        }
      }
    };

    ///карта пеерхватов, соответствие старых адресов функций новым емуляторам
    typedef std::map<CodeAddress,EmulatorBegin*> EmBeginMap;
    EmBeginMap m_embeginmap;
    /// чтобы была возможность отменить перехват выхода из фукнции я хранию
    /// указатели на временные обьекты EmulatorEnd
    typedef std::set<EmulatorEnd*> EmEndMap;
    EmEndMap m_emendmap;

    /// структурка делает вызов функции которая должна
    /// вызываться.
    /// для CatchImportFunction это будет адрес настоящей
    ///   процедуры вызов которой перехватывуют
    ///   скажем я хочу знать когда вызывали ShowWindow
    ///   я могу изменить входные параметры и выходной результа

    struct EmulatorImport
    {
      const unsigned char push_ret;
      Proc Proc;
      const unsigned char ret;

      EmulatorImport() : push_ret(0x68),ret(0xc3) {}
    };

    /// для RedirectImportFunction это адрес процедуры
    ///   которую хотят переназначить
    struct EmulatorRedirectImport
    {
      const unsigned char ret;

      unsigned char reverved[sizeof(EmulatorBegin)];

      EmulatorRedirectImport() : ret(0xc3) {}
    };

  #pragma pack(pop)

    ///передаеться адрес старой процедуры, процедура находит емулятор и включает его
    void EnableEmulator(CodeAddress jmp);
    /// retaddress - адрес возврата
    ///   внешний код вызывает функцию, и в стек кладет адрес возврата - это он.
    ///   call xxx; -> push retaddress,jmp xxx;
    ///   retaddress: nextoperand;
    static void __stdcall CreateEmulator(CMemMngr* client,CodeAddress j,char* p,CodeAddress* retaddress);
    static void __stdcall FreeEmulator(CMemMngr* client,EmulatorEnd*,int* retval);
    /// эта функция вызывается когда ловиться EmulatroBegin*
    static void __stdcall CallCatchBegin(CMemMngr*,Jmp *);
    /// эта функция вызывается когда ловиться EmulatroEnd*
    static void __stdcall CallCatchEnd(CMemMngr*,void*);
  };
}

#endif
