// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <atlbase.h>

#include <string>
#include <algorithm>

#include "emulator.h"

#include <debugger/debug.h>
#include <misc/SentrySc.h>

// Assist {

static std::string GetFileName(const char* path)
{
  char drive[_MAX_DRIVE];
  char dir[_MAX_DIR];
  char fname[_MAX_FNAME];
  char ext[_MAX_EXT];
  _splitpath(path,drive,dir,fname,ext);

  return std::string(fname)+ext;
}

static std::string GetModuleName(HMODULE hmodule)
{
  char buf[MAX_PATH];
  GetModuleFileName((HMODULE)hmodule,buf,sizeof(buf));
  return GetFileName(buf);
}

static BOOL IsBadCodePtr_my(LPDWORD f)
{
  MEMORY_BASIC_INFORMATION mbi;
  if(VirtualQuery(f,&mbi,sizeof(mbi))==0)
    return TRUE;

  if(mbi.State!=MEM_COMMIT)
    return TRUE;
  if(mbi.Type==MEM_PRIVATE)
    return TRUE;
  
  if(
    /*mbi.AllocationProtect&PAGE_READONLY||
    mbi.AllocationProtect&PAGE_READWRITE||
    mbi.AllocationProtect&PAGE_EXECUTE_READ||
    mbi.AllocationProtect&PAGE_EXECUTE_READWRITE||*/
    mbi.Protect&PAGE_READONLY||
    mbi.Protect&PAGE_READWRITE||
    mbi.Protect&PAGE_EXECUTE_READ||
    mbi.Protect&PAGE_EXECUTE_READWRITE)
  {
    try
    {
      DWORD d=*f;
    }catch(...)
    {
      return TRUE;
    }
    return FALSE;
  }else
  {
    return TRUE;
  }
}

// } Assist

// CEmulator {

IOFluke::CEmulator::CEmulator()
{
}

void IOFluke::CEmulator::Create(HookList *h)
{
  SentrySc sc(m_cs);
  m_hooklist=h;
}

IOFluke::CEmulator::CEmulator(HookList *h)
{
  SentrySc sc(m_cs);
  m_hooklist=h;
}

IOFluke::hook* IOFluke::CEmulator::GetHook(DWORD implold)
{
  SentrySc sc(m_cs);
  HookList::iterator i=m_hooklist->begin();
  while(i!=m_hooklist->end())
  {
    if(i->implold==implold)
      return &*i;
    i++;
  }
  return 0;
}

void IOFluke::CEmulator::ParseImport(const char* bodydllname,PIMAGE_THUNK_DATA thunk)
{
  SentrySc sc(m_cs);
  while(thunk->u1.Function!=0)
  {
    if(hook* h=GetHook((DWORD)thunk->u1.Function))
    {
      DBGTRACE("create hook %s %s\n",bodydllname,h->oldfuncname.c_str());
      DWORD old;
      VirtualProtect(&thunk->u1.Function,4,PAGE_READWRITE,&old);
      m_roolbacklist.push_back(roolback_t(&thunk->u1.Function,thunk->u1.Function));
      thunk->u1.Function=(DWORD)h->implnew;
      VirtualProtect(&thunk->u1.Function,4,old,&old);
    }
    thunk++;
  }
}

void IOFluke::CEmulator::ParseModule(HMODULE module)
{
  SentrySc sc(m_cs);
  PIMAGE_DOS_HEADER idh=(PIMAGE_DOS_HEADER)module;
  PIMAGE_NT_HEADERS inh=(PIMAGE_NT_HEADERS)((DWORD)idh+idh->e_lfanew);
  if((*inh).Signature=='EP')
  {
    char dllname[MAX_PATH];
    GetModuleFileName((HMODULE)idh,dllname,sizeof(dllname));
    /*if(inh->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IAT].Size!=0)
    {
      PIMAGE_THUNK_DATA thunk=(PIMAGE_THUNK_DATA)((DWORD)idh+inh->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IAT].VirtualAddress);
      ParseImport(dllname,thunk);
    }*/
    if(inh->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size!=0)
    {
      PIMAGE_IMPORT_DESCRIPTOR import=(PIMAGE_IMPORT_DESCRIPTOR)((DWORD)idh+inh->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
      while((*import).Name!=0)
      {
        const char* p=(const char*)((DWORD)import->Name+(DWORD)idh);
        PIMAGE_THUNK_DATA thunk=(PIMAGE_THUNK_DATA)((DWORD)idh+import->FirstThunk);
        ParseImport(dllname,thunk);
        import++;
      }
    }
  }
}

void IOFluke::CEmulator::ParseMemory(HINSTANCE curinstance)
{
  SentrySc sc(m_cs);

  DBGTRACE("ParseMemory\n");

  std::string &curmodname=GetModuleName(curinstance);

  DWORD address=0x00400000;
  while(address<0x7fffffff)
  {
    PIMAGE_DOS_HEADER idh=(PIMAGE_DOS_HEADER)address;
    if(!IsBadCodePtr_my((LPDWORD)address)&&
      idh->e_magic=='ZM')
    {
      std::string &modname=GetModuleName((HMODULE)address);
      if(modname!=curmodname)
      {
        DBGTRACE("parse '%s'\n",modname.c_str());
        ParseModule((HMODULE)idh);
      }
    }
    address+=0x1000;
  }
}

IOFluke::CEmulator::~CEmulator()
{
  //в деструкторе особождать память поздно :( (для глобальных обьектов)
  //дело в том что kernel32 сначало запрашивает адрес ZvTerminateProcess
  //потом освобождает эму бибилотеку, а только затем вызвает ^^^
  //из за этого указатель идет на unaccessed memory

  Close();
}

void IOFluke::CEmulator::Close()
{
  SentrySc sc(m_cs);

  for(roolbacklist_t::iterator i=m_roolbacklist.begin();i!=m_roolbacklist.end();i++)
  {
    unsigned long old;
    VirtualProtect(i->address_mem,4,PAGE_READWRITE,&old);
    *(i->address_mem)=i->address_proc;
    VirtualProtect(i->address_mem,4,old,&old);
  }
}

// } CEmulator
