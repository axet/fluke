// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/// @file
/// @author Alexey Kuznetsov (axet@nm.ru)

/// Модуль для перехвата фукнциями ввода вывода .

///CMemMngr класс разработан для перехвата функций в коде программы,и
///перехвата выхода из этих функций
///CEmulator класс предназначеный для перехвата функций импорта.
///
///программно построено так что разницы не заметно что я делую
///перехват импортируемой или функции в коде программно, создаеться
///впечетление что перехват сделан не удобно, в действительности
///это последствия не удачного объеденения этих двух фукнций.
///
///сделать удобнее не предплогаеться возможнее, поскольку перед
///вызовом будет проблематично встставновить правильный esp
/// <PRE>
/// /////////////////////////////////////////////
/// 
/// //пример работы для функций 
/// 
/// //CMemMngr::CatchImportFunction()
/// 
/// /////////////////////////////////////////////
/// 
/// static void hook_accept(CMemMngr* mngr,va_list )
/// 
/// void CFirewall::hook_accept(CMemMngr* mngr,va_list )
/// {
///   CFirewall* current=dynamic_cast<CFirewall*>(mngr);
/// 
///   SOCKET s=va_arg(args,SOCKET);
///   current->MemberFn(p);
/// }
/// /////////////////////////////////////////////
/// </PRE>

#include "emulator.h"
#include "memmngr.h"

namespace IOFluke
{
};
