// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef EMULATOR_H
#define EMULATOR_H

#include <windows.h>
#include <atlbase.h>

#include <vector>
#include <utility>
#include <list>

#include <debugger/debug.h>

namespace IOFluke
{
  /// Хок на фукнцию.
  struct hook
  {
    /// имя библиотеки в которой находиться перехватываемая фукнция
    std::string oldmodulename;
    /// имя фукнции которунж нужно перехватить
    std::string oldfuncname;
    ///адрес страой процедуры (процедура в длл impladdress
    DWORD implold;

    ///адрес новой процедуры (процедуря для котроля)
    DWORD implnew;

    /// НЕЗАБЫТЬ ПОСТАВИТЬ ВЕРНОЕ СОГЛАЖЕНИЕ О ВЫЗОВЕ!
    /// @param _implnew адрес фукнции куда переналпавлять вызов
    /// @param dllname имя библиотеки в которой находиться перехватываемая фукнция
    /// @param funcname имя фукнции которунж нужно перехватить
    hook(void *_implnew,const char* dllname,const char* funcname)
    {
      oldmodulename=dllname;
      oldfuncname=funcname;
      implold=(DWORD)GetProcAddress(GetModuleHandle(dllname),funcname);
      if(implold==0)
        DBGTRACE("emulator: in '%s' not found '%s'\n",dllname,funcname);
      implnew=(DWORD)_implnew;
    }
  };

  /// Список хоков.
  /// @sa hook
  class HookList:public std::vector< hook > {};

  /// CEmulator - анализатор памяти, находит вызовы импортируемых функций

  /// класс для перехвата всех внешних вызовов, глючит на MFC
  /// из за начличия кривого експорта на переменную.
  /// implold - implementation old, та что была
  /// implnew - implementation new, та кем стала

  class CEmulator
  {
    /// информация которую мы сохраняем
    typedef struct roolback_tag
    {
      /// адрес памяти на котороый идет запись
      unsigned long* address_mem;
      /// адрес старой фукнции которая должна вызваться
      unsigned long address_proc;

      roolback_tag(unsigned long* mem,unsigned long Proc):address_mem(mem),address_proc(Proc) {;}
    } roolback_t;
    typedef std::list<roolback_t> roolbacklist_t;

    /// список адреов на память которые были заменены
    roolbacklist_t m_roolbacklist;
    /// указатель на массив хоков которые нужно поставить
    HookList* m_hooklist;
    CComAutoCriticalSection m_cs;

    hook* GetHook(DWORD implold);
  public:
    CEmulator();
    /// создать эмулятор фукнций которые необходимо перехватывать
    CEmulator(HookList*);
    ~CEmulator();
    /// создать эмулятор фукнций которые необходимо перехватывать
    void Create(HookList*);
    /// откатить перехват фукнций
    void Close();
    /// обработать секцию импорта указанной библиотеки
    void ParseImport(const char* bodydllname,PIMAGE_THUNK_DATA thunk);
    /// обработать указанный модуль на наличие фукнций хока
    void ParseModule(HMODULE module);
    /// обработать указаанную область памяти на наличие фукнций хока
    void ParseMemory(HINSTANCE curinstance);
  };
}

#endif
