// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <atlbase.h>
#include <assert.h>
#include <new>

#include "memmngr.h"
#include "emulator.h"

#include <misc/SentrySc.h>

IOFluke::CMemMngr::~CMemMngr()
{
  SentrySc sc(m_cs);

  m_emulator.Close();

  for(EmBeginMap::iterator i=m_embeginmap.begin();i!=m_embeginmap.end();i++)
    delete i->second;
  for(IOFluke::HookList::iterator i=m_hl.begin();i!=m_hl.end();i++)
    delete (EmulatorImport*)i->implnew;
  m_embeginmap.clear();
  for(EmEndMap::iterator i=m_emendmap.begin();i!=m_emendmap.end();i++)
    delete *i;
  m_emendmap.clear();

  delete &m_hl;
  delete &m_emulator;

  for(DllList::iterator i=m_dlllist.begin();i!=m_dlllist.end();i++)
    FreeLibrary(*i);
}

IOFluke::CMemMngr::CMemMngr():
  m_emulator(*new CEmulator()),
  m_hl(*new HookList())
{
  SentrySc sc(m_cs);
  m_emulator.Create(&m_hl);
}

void IOFluke::CMemMngr::RedirectImportFunction(const char* funcname,const char* dllname,CatchProcEnter funcEnter,CatchProcLeave funcLeave,Proc stubLeave)
{
  SentrySc sc(m_cs);

  m_dlllist.push_back(LoadLibrary(dllname));

  EmulatorImport *p=new EmulatorImport;
  p->Proc=stubLeave;
  m_hl.push_back(IOFluke::hook(p,dllname,funcname));
  CatchRetFunction((unsigned)p,funcEnter,funcLeave);

  m_emulator.ParseModule(GetModuleHandle(0));
}

void IOFluke::CMemMngr::CatchImportFunction(const char* funcname,const char* dllname,CatchProcEnter funcEnter,CatchProcLeave funcLeave)
{
  SentrySc sc(m_cs);

  m_dlllist.push_back(LoadLibrary(dllname));

  EmulatorImport *p=new EmulatorImport;
  p->Proc=(Proc)GetProcAddress(GetModuleHandle(dllname),funcname);
  m_hl.push_back(IOFluke::hook(p,dllname,funcname));
  CatchRetFunction((unsigned)p,funcEnter,funcLeave);

  m_emulator.ParseModule(GetModuleHandle(0));
}

void IOFluke::CMemMngr::CatchRetFunction(CodeAddress oldaddr,CatchProcEnter newaddr,CatchProcLeave funcLeave)
{
  SentrySc sc(m_cs);

  EmulatorBegin* e=new EmulatorBegin;
  e->client=this;
  e->CallCatch=(Proc)CallCatchBegin;
  e->funcEnter=newaddr;
  e->funcLeave=funcLeave;
  m_embeginmap[oldaddr]=e;
  EnableEmulator(oldaddr);
}

void IOFluke::CMemMngr::EnableEmulator(CodeAddress jmp)
{
  SentrySc sc(m_cs);
  EmulatorBegin *e=m_embeginmap[jmp];
  DWORD old;
  e->retaddr=jmp;
  e->backdata=*(Jmp*)jmp;
  VirtualProtect((Jmp*)jmp,sizeof(Jmp),PAGE_EXECUTE_READWRITE,&old);
  *(Jmp*)jmp=Jmp(e);
}

void IOFluke::CMemMngr::CreateEmulator(CMemMngr* client,CodeAddress j,char* p,CodeAddress *retaddress)
{
  SentrySc sc(client->m_cs);

  EmulatorBegin* em;
  EmulatorEnd* e;
  va_list args;

  em=((Jmp*)j)->e;

  // восстнавливую исходный код
  *(Jmp*)j=((Jmp*)j)->e->backdata;

  // вызываю нового обработчика для этой фукнции
  va_start(args,*p);
  if(em->funcEnter!=0)
  {
    sc.Unlock();
    em->funcEnter(client,args);
    sc.Lock();
  }
  va_end(args);

  // создаю перехват на выходе из перехватываемой функции
  e=new EmulatorEnd();
  e->catch_addr=j;
  e->retaddr=*retaddress;
  e->client=client;
  e->CallCatch=(Proc)CallCatchEnd;
  e->funcLeave=em->funcLeave;
  e->stacklink=retaddress;

  *retaddress=(CodeAddress)e;

  client->m_emendmap.insert(e);
}

void IOFluke::CMemMngr::FreeEmulator(CMemMngr* client,EmulatorEnd*e,int *retval)
{
  SentrySc sc(client->m_cs);

  if(e->funcLeave!=0)
    e->funcLeave(client,retval);

  client->EnableEmulator(e->catch_addr);

  e->stacklink=0;

  client->m_emendmap.erase(e);

  delete e;
}

void IOFluke::CMemMngr::PatchProgram(DWORD addr,const void* buf,int size)
{
  DWORD old;
  VirtualProtect((LPVOID)addr,size,PAGE_EXECUTE_READWRITE,&old);
  memcpy((void*)addr,buf,size);
  VirtualProtect((LPVOID)addr,size,old,&old);
}

// disable optimisation

#pragma optimize("",off)
void __declspec(naked) IOFluke::CMemMngr::CallCatchBegin(CMemMngr* client,Jmp * j)
{
  __asm
  {
    push ebp;
    mov ebp,esp;

    push eax;
    push edx;
    push ecx;
    push esi;

    // адрес возврата тот в который возвращает управление перехватываемая функция
    lea eax,[ebp+0x4*4];
    push eax
    // адрес первого параметра для перехватываемой функции
    // так как будет удобно работать с параметрами через vs_args то
    // я передаю адресс первого параметра минус 4 байта
    lea eax,[ebp+0x4*4];
    push eax
    // адрес перехватываемой процедуры
    push [j]
    // this
    push [client];
    call CreateEmulator;

    // enable emulator

    // подменяю адрес возврата, чтобы потом перехватить выход из перехватываемой
    // процедуры
    // mov [ebp+0x4*4],eax;

    // возвращаю управление коду который должен выполниться
    mov eax,[j];
    mov dword ptr [ebp+4],eax;

    pop esi;
    pop ecx;
    pop edx;
    pop eax;

    mov esp,ebp;
    pop ebp;
    ret 0x8;
  }
}

void __declspec(naked) IOFluke::CMemMngr::CallCatchEnd(CMemMngr* client,void *retaddr)
{
  __asm{
    push ebp;
    mov ebp,esp;

    sub esp,04h;

    push eax;
    push ebx;
    push edx;
    push ecx;
    push esi;
  
    // возвращаемое значение из функции
    lea ebx,[ebp-0x4]
    mov [ebx],eax
    push ebx;
    // указатель на эмелятор
    mov eax,[ebp+4]; // virtual_ret
    sub eax,0xf; // 0x0f - длинна данных в стеке которые кладет моя программа
    push eax;
    push [client];
    call FreeEmulator;

    mov eax,[retaddr];
    mov dword ptr [ebp+4],eax;

    pop esi;
    pop ecx;
    pop edx;
    pop ebx;
    pop eax;

    mov eax,[ebp-0x4]

    add esp,4

    mov esp,ebp;
    pop ebp;
    ret 0x8;
  }
}
